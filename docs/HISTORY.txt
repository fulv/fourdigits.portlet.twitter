Changelog
=========

1.0a6 (unreleased)
------------------

- Twitter API (twitter.py) updated to 0.8.3
- Added proxy and timeout information to Twitter API
- Added Control Panel to the Product (Thanks to Manabu TERADA - https://bitbucket.org/terapyon)
- Added cache for UserInfo and Tweets (Thanks to Manabu TERADA - https://bitbucket.org/terapyon)
- Updated translation to PT-BR

1.0a5 (unreleased)
------------------

- Fixed search for multiple terms. Now combine search terms [cekk]
- Added "from_user" argument in twitter library to allow combined search between userid and text [cekk]
- Added field in portlet config to allow combined search [cekk] 
- Fixed translation message ids [cekk]
- Added italian translation [cekk]

1.0a4 (2012-09-27)
------------------

- Return only first part of language in case of nl-nl, we only register nl.js files [kingel]


1.0a3 (2012-09-05)
------------------

- Replace jquery.easydate.js for collective.js.moment, remove not needed
  javascripts from portal_javascript. [kingel]


1.0a2 (2012-08-10)
------------------
- Make sure old portlet assignments keep working [ralphjacobs]


1.0a (2012-08-10)
-----------------
- Added support for clientside rendering using the libs from http://tweet.seaofclouds.com/
  You can turn this on in the portlet settings.
  (note: relative dates are in dutch only at the moment) [kingel]
- Fixed serverside caching by using a os.getpid() for generating tmp dir to avoid permission problems [ralphjacobs]

0.9.2
-----
- Adding original permission for Site Administrator [terapyon]
- Adding test code for portlet base [terapyon]

0.9.1
-----
- Fixed missing files in 0.9 package [sjoerdve]

0.9
---
- Fix invalid CSS [sjoerdve]

0.8
---
- Include alt text for twitter images [ralphjacobs]

0.7
---
- Made portlet backwards compatible with Plone 3 versions again [ralphjacobs]

0.6
---
- Added support for Plone 4.1 [sjoerdve]
- Fixed double name bug when tweets are searched [sjoerdve]
- Minor code cleanup [ralphjacobs]

0.5
---
- Added support for displaying twitter user profile info [ralphjacobs]
- Fixed caching bug, fixes ticket #4 [ralphjacobs]
- Based on new upstream python-twitter library 0.8.2 [ralphjacobs]
- Made implemetation compatible with new upstream library [ralphjacobs]
- Added multilanguage support [ralphjacobs] [rafahela]
- Added dutch translations [ralphjacobs]
- Added Portugese (Brazilian) translations [rafahela]

0.4
---
- Added classes to fields in template. [robgietema]
- Removed markup from python files [robgietema]
- Extended matching regext for auto-link URLs [ralphjacobs]

0.3
---
- Added support for retweets based on username [ralphjacobs]

0.2
---
- Fix release, refactored tweet results logic [martijn4d]

0.1
---
- Initial version, based on collective.twitter
  and python-twitter 0.7-devel for searching
- (Multi) Language support for twitter feeds
- Better text filter for curse words
- Multi search support
- User picture support
